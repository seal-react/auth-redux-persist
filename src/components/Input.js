import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Pressable,
  Platform,
} from 'react-native';
import {colors, dimens, fonts} from '../utils';

const Input = ({
  autoCapitalize,
  type, //password, phone
  onChangeText,
  value,
  placeholder,
  placeholderTextColor,
  keyboardType,
  label,
  customStyleInput,
  customContainerStyle,
  editable = true,
  multiline,
  customContainerInputStyle,
  onPress,
  LeftIcon,
  leftIconStyle,
  leftIconPress,
  RightIcon,
  rightIconStyle,
  rightIconPress,
  numberOfLines,
  withoutShadow,
  onFocus,
  onBlur,
  autoFocus,
  errorText,
  onSubmitEditing,
  returnKeyType,
}) => (
  <Pressable style={customContainerStyle} onPress={onPress}>
    {label && (
      <Text allowFontScaling={false} style={styles.label}>
        {label || 'label'}
      </Text>
    )}
    <View
      style={[
        styles.containerInput,
        withoutShadow ? styles.withoutShadow : {},
        // editable ? {} : styles.inputContainerDisabled,
        customContainerInputStyle,
      ]}>
      {!!LeftIcon && (
        <LeftIcon
          style={[styles.leftIconStyle, leftIconStyle]}
          onPress={leftIconPress}
        />
      )}
      <TextInput
        autoFocus={autoFocus}
        multiline={multiline}
        numberOfLines={numberOfLines}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
        value={value}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor || colors.gray1}
        editable={editable}
        secureTextEntry={type === 'password'}
        style={[
          styles.input,
          {color: colors.black1, paddingHorizontal: dimens[8]},
          LeftIcon ? {paddingLeft: dimens[28]} : {},
          Platform.OS === 'ios' ? {marginTop: 0} : {},
          customStyleInput,
        ]}
        autoCapitalize={autoCapitalize}
        onFocus={onFocus}
        onBlur={onBlur}
        allowFontScaling={false}
        returnKeyType={returnKeyType || 'default'}
      />
      {!!RightIcon && (
        <RightIcon
          style={[styles.rightIconStyle, rightIconStyle]}
          onPress={rightIconPress}
        />
      )}
    </View>
    {errorText && (
      <Text style={styles.errorText} allowFontScaling={false}>
        {errorText}
      </Text>
    )}
  </Pressable>
);

export default Input;

const styles = StyleSheet.create({
  inputContainer: {
    height: dimens[42],
    flexDirection: 'row',
    paddingHorizontal: dimens[8],
    // shadow ios
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // shadow android
    elevation: 3,
    backgroundColor: 'white',
    borderRadius: 3,
    alignItems: 'center',
  },
  inputContainerDisabled: {
    opacity: 0.5,
  },
  containerInput: {
    height: dimens[42],
    paddingHorizontal: dimens[8],
    backgroundColor: 'white',
    borderRadius: 3,
    // shadow ios
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // shadow android
    elevation: 3,
    borderColor: colors.gray1,
    borderWidth: 1,
    marginBottom: dimens[16],
  },
  withoutShadow: {
    // shadow ios
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,
    // shadow android
    elevation: 0,
  },
  wrapAreaCode: {
    flexDirection: 'row',
  },
  areaCode: {
    fontFamily: fonts.medium,
    color: colors.black1,
    marginTop: 4,
  },
  separator: {
    height: 20,
    width: 1,
    backgroundColor: colors.gray1,
    marginLeft: dimens[8],
    marginTop: 4,
  },
  input: {
    height: '100%',
    width: '100%',
    paddingHorizontal: dimens[12],
    fontFamily: fonts.normal,
    color: colors.black1,
    marginTop: 6,
    lineHeight: dimens[20],
    fontSize: dimens[14],
    paddingTop: 0,
  },
  label: {
    fontFamily: fonts.medium,
    fontSize: dimens[16],
    lineHeight: dimens[24],
    color: colors.black1,
    marginBottom: dimens[4],
  },
  leftIconStyle: {
    height: dimens[24],
    width: dimens[24],
    resizeMode: 'contain',
    position: 'absolute',
    left: dimens[12],
    top: dimens[12],
  },
  rightIconStyle: {
    height: dimens[24],
    width: dimens[24],
    resizeMode: 'contain',
    position: 'absolute',
    right: dimens[12],
    top: dimens[12],
  },
  errorText: {
    fontFamily: fonts.normal,
    fontSize: dimens[12],
    color: colors.red,
    marginTop: -dimens[12],
  },
});
