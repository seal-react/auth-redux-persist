import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {useSelector} from 'react-redux';

import Login from '../screens/Login';
import Home from '../screens/Home';

const Routes = () => {
  const user = useSelector(state => state.user);

  return <View style={{flex: 1}}>{user ? <Home /> : <Login />}</View>;
};

export default Routes;
