import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View, StatusBar} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../components/Button';
import Gap from '../components/Gap';
import {dimens} from '../utils';
import {setUser} from '../store/actionCreators';

const Home = () => {
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();
  const logout = async values => {
    dispatch(setUser(null));
    alert('Success logout');
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Gap bottom={dimens[20]} />
          <Text style={styles.welcome}>Welcome, {user?.email || 'Guest'}</Text>
          <Gap bottom={dimens[20]} />
          <Button title="Logout" onPress={logout} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: dimens[20],
  },
  welcome: {
    fontSize: 22,
    color: '#000',
  },
});
