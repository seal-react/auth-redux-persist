import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View, StatusBar} from 'react-native';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {useDispatch} from 'react-redux';
import Input from '../components/Input';
import Button from '../components/Button';
import Gap from '../components/Gap';
import {dimens} from '../utils';
import {setUser} from '../store/actionCreators';

const FormSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email format is invalid')
    .required('Email is required'),
  password: Yup.string().required('Password is required'),
});

const Login = () => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: FormSchema,
    onSubmit: values => submit(values),
  });

  const submit = async values => {
    dispatch(setUser(values));
    alert(JSON.stringify(values));
    alert('Success login');
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Input
            label="Email"
            placeholder="name@email.com"
            value={formik.values.email}
            onChangeText={text => formik.setFieldValue('email', text)}
            withoutShadow
            autoCapitalize="none"
            errorText={formik.errors.email}
          />
          <Input
            label="Password"
            type="password"
            placeholder="Input password here"
            value={formik.values.password}
            onChangeText={text => formik.setFieldValue('password', text)}
            withoutShadow
            autoCapitalize="none"
            errorText={formik.errors.password}
          />
          <Gap bottom={dimens[20]} />
          <Button
            isDisabled={!formik.isValid}
            title="Login"
            onPress={formik.handleSubmit}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: dimens[20],
  },
});
