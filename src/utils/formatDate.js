export const formatMonth = month => {
  if (month <= 9) {
    return `0${month}`;
  }
  return month;
};

export const formatDate = date => {
  const d = new Date(date);
  const day = d.getDate();
  const month = d.getMonth(); //date month start index with 0
  const year = d.getFullYear();

  return `${formatMonth(day)}/${formatMonth(month + 1)}/${year}`;
};
