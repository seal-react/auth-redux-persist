export const toCurrency = amount => {
  return amount > 0
    ? 'Rp ' + amount?.toString()?.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
    : 'Rp 0';
};

export const capitalizeFirstLetter = string => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const elipsis = (text, index) => {
  return text.length > index ? text.substring(0, index) + '...' : text;
};
